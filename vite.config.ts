import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react'
// https://fe-developers.kakaoent.com/2022/220217-learn-babel-terser-swc/

// https://vitejs.dev/config/
export default ({ mode }) => {
  // https://minu0807.tistory.com/121
  // https://vitejs-kr.github.io/guide/env-and-mode.html
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) }
  return defineConfig({
    plugins: [
      react({
        jsxImportSource: '@emotion/react',
        babel: {
          plugins: ['@emotion/babel-plugin'],
        },
      }),
    ],
    server: {
      watch: {
        usePolling: true,
      },
      port: 3000,
    },
    base: `${process.env.VITE_ROOT_PATH}/`,
  })
}
