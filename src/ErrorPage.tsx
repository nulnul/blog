import React from 'react'
import { useRouteError } from 'react-router-dom'

type ErrorResponse = {
  data: any
  status: number
  statusText: string
  message?: string
}

const errorCheck = (error: any): error is ErrorResponse => {
  return ('data' in error && 'status' in error && 'statusText' in error) || 'message' in error
}

export default function ErrorPage() {
  const error: any = useRouteError()
  console.error(error)

  if (errorCheck(error)) {
    return (
      <div id='error-page'>
        <h1>아이쿠!</h1>
        <p>죄송합니다. 예상치 못한 오류가 발생했습니다.</p>
        <p>
          <i>{error.statusText || error?.message}</i>
        </p>
      </div>
    )
  } else {
    return (
      <>
        <div id='error-page'>
          <h1>에러가 아닌가!?</h1>
          <p>죄송합니다. 조금 더 확인해보겠습니다.</p>
          <p>
            <i>{error.statusText || error?.message}</i>
          </p>
        </div>
      </>
    )
  }
}
