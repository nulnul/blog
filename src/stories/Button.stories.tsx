import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Button } from '@/components/atomic/Button'

export default {
  title: 'Atomic/Button',
  component: Button,
  argTypes: {},
} as ComponentMeta<typeof Button>

export const Default: ComponentStory<typeof Button> = (args) => {
  return (
    <>
      <h1>Button</h1>
      <div className='explain'>
        <ul>
          <li>
            <b>type?:</b> button | submit | reset
          </li>
          <li>
            <b>size?:</b> 46 | 44 | 32(default) | 26
          </li>
          <li>
            <b>buttonColorType?:</b> primary | secondary | success | danger | warning | info | light
            | dark | link
          </li>
        </ul>
      </div>

      <h1>Size - 46 primary</h1>
      <br />

      <Button {...args} size={46}>
        버튼(Primary)
      </Button>
      <Button {...args} disabled size={46}>
        Disabled(Primary)
      </Button>
      <br />

      <h1>Size - 46 secondary</h1>
      <br />

      <Button {...args} buttonColorType='secondary' size={46}>
        Secondary
      </Button>
      <Button {...args} disabled buttonColorType='secondary' size={46}>
        Disabled(Secondary)
      </Button>
      <br />

      <h1>Size - 46 success</h1>
      <br />

      <Button {...args} buttonColorType='success' size={46}>
        Success
      </Button>
      <Button {...args} disabled buttonColorType='success' size={46}>
        Disabled(Success)
      </Button>
      <br />

      <h1>Size - 46 danger</h1>
      <br />

      <Button {...args} buttonColorType='danger' size={46}>
        Danger
      </Button>
      <Button {...args} disabled buttonColorType='danger' size={46}>
        Disabled(Danger)
      </Button>
      <br />

      <h1>Size - 46 warning</h1>
      <br />

      <Button {...args} buttonColorType='warning' size={46}>
        Warning
      </Button>
      <Button {...args} disabled buttonColorType='warning' size={46}>
        Disabled(Warning)
      </Button>
      <br />

      <h1>Size - 46 info</h1>
      <br />

      <Button {...args} buttonColorType='info' size={46}>
        Info
      </Button>
      <Button {...args} disabled buttonColorType='info' size={46}>
        Disabled(Info)
      </Button>
      <br />

      <h1>Size - 46 light</h1>
      <br />

      <Button {...args} buttonColorType='light' size={46}>
        Light
      </Button>
      <Button {...args} disabled buttonColorType='light' size={46}>
        Disabled(Light)
      </Button>
      <br />

      <h1>Size - 46 dark</h1>
      <br />

      <Button {...args} buttonColorType='dark' size={46}>
        Dark
      </Button>
      <Button {...args} disabled buttonColorType='dark' size={46}>
        Disabled(Dark)
      </Button>
      <br />

      <h1>Size - 46 link</h1>
      <br />

      <Button {...args} buttonColorType='link' size={46}>
        Link
      </Button>
      <Button {...args} disabled buttonColorType='link' size={46}>
        Disabled(Link)
      </Button>
    </>
  )
}
/*

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Primary = Template.bind({})
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  label: 'Button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  label: 'Button',
  buttonColorType: 'secondary',
}
*/
