import { Form } from 'react-router-dom'
import React, { PropsWithChildren } from 'react'
import { ContactType } from './Contact'

export function Favorite({ favorite }: PropsWithChildren<ContactType>) {
  // yes, this is a `let` for later
  return (
    <Form method='post'>
      <button
        name='favorite'
        value={favorite ? 'false' : 'true'}
        aria-label={favorite ? 'Remove from favorites' : 'Add to favorites'}
      >
        {favorite ? '★' : '☆'}
      </button>
    </Form>
  )
}
