export const Colors = {
  PRIMARY: '#007bff',
  SECONDARY: '#6c757d',
  SUCCESS: '#28a745',
  DANGER: '#dc3545',
  WARNING: '#ffc107',
  INFO: '#17a2b8',
  LIGHT: '#f8f9fa',
  DARK: '#343a40',
  WHITE: '#fff',
  DARK_GRAY: '#212529',
  BLACK: '#000',

  PRIMARY_2: 'rgba(0, 123, 255, 0.02)',
  PRIMARY_20: 'rgba(0, 123, 255, 0.2)',
  PRIMARY_50: 'rgba(0, 123, 255, 0.5)',
  PRIMARY_70: 'rgba(0, 123, 255, 0.7)',

  SECONDARY_2: 'rgba(108, 117, 125, 0.02)',
  SECONDARY_20: 'rgba(108, 117, 125, 0.2)',
  SECONDARY_50: 'rgba(108, 117, 125, 0.5)',
  SECONDARY_70: 'rgba(108, 117, 125, 0.7)',

  SUCCESS_2: 'rgba(40, 167, 69, 0.02)',
  SUCCESS_20: 'rgba(40, 167, 69, 0.2)',
  SUCCESS_50: 'rgba(40, 167, 69, 0.5)',
  SUCCESS_70: 'rgba(40, 167, 69, 0.7)',

  DANGER_2: 'rgba(220, 53, 69, 0.02)',
  DANGER_20: 'rgba(220, 53, 69, 0.2)',
  DANGER_50: 'rgba(220, 53, 69, 0.5)',
  DANGER_70: 'rgba(220, 53, 69, 0.7)',

  WARNING_2: 'rgba(225, 193, 7, 0.02)',
  WARNING_20: 'rgba(225, 193, 7, 0.2)',
  WARNING_50: 'rgba(225, 193, 7, 0.5)',
  WARNING_70: 'rgba(225, 193, 7, 0.7)',

  INFO_2: 'rgba(23, 162, 184, 0.02)',
  INFO_20: 'rgba(23, 162, 184, 0.2)',
  INFO_50: 'rgba(23, 162, 184, 0.5)',
  INFO_70: 'rgba(23, 162, 184, 0.7)',

  LIGHT_2: 'rgba(248, 249, 250, 0.02)',
  LIGHT_20: 'rgba(248, 249, 250, 0.2)',
  LIGHT_50: 'rgba(248, 249, 250, 0.5)',
  LIGHT_70: 'rgba(248, 249, 250, 0.7)',

  DARK_2: 'rgba(52, 58, 64, 0.02)',
  DARK_20: 'rgba(52, 58, 64, 0.2)',
  DARK_50: 'rgba(52, 58, 64, 0.5)',
  DARK_70: 'rgba(52, 58, 64, 0.7)',

  WHITE_2: 'rgba(255, 255, 255, 0.02)',
  WHITE_20: 'rgba(255, 255, 255, 0.2)',
  WHITE_50: 'rgba(255, 255, 255, 0.5)',
  WHITE_70: 'rgba(255, 255, 255, 0.7)',

  DARK_GRAY_2: 'rgba(33, 47, 41, 0.02)',
  DARK_GRAY_20: 'rgba(33, 47, 41, 0.2)',
  DARK_GRAY_50: 'rgba(33, 47, 41, 0.5)',
  DARK_GRAY_70: 'rgba(33, 47, 41, 0.7)',
}
