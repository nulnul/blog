import { css } from '@emotion/react'

export enum StyleFontWeight {
  LIGHT = 300,
  REGULAR = 400,
  BOLD = 900,
}

export const DEFAULT_FONTS = css`
  @font-face {
    font-family: 'Nunito Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
`

export const FONT_FAMILY_SANS_SERIF =
  '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen","Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif'

export const makeFonts = (
  fontSize: string,
  fontWeight: number | string,
  lineHeight: number | string
) => {
  return css`
    font-size: ${fontSize};
    font-weight: ${fontWeight};
    line-height: ${lineHeight};
  `
}

export const Fonts = {
  BOLD_24: makeFonts('24px', StyleFontWeight.BOLD, '1.4'),
  BOLD_22: makeFonts('22px', StyleFontWeight.BOLD, '1.4'),
  BOLD_20: makeFonts('20px', StyleFontWeight.BOLD, '1.4'),
  BOLD_18: makeFonts('18px', StyleFontWeight.BOLD, '1.4'),
  BOLD_16: makeFonts('16px', StyleFontWeight.BOLD, '1.4'),
  BOLD_14: makeFonts('14px', StyleFontWeight.BOLD, '1.4'),
  BOLD_13: makeFonts('13px', StyleFontWeight.BOLD, '1.4'),
  BOLD_12: makeFonts('12px', StyleFontWeight.BOLD, '1.4'),

  REGULAR_20: makeFonts('20px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_18: makeFonts('18px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_16: makeFonts('16px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_14: makeFonts('14px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_13: makeFonts('13px', StyleFontWeight.REGULAR, '1.4'),
  REGULAR_12: makeFonts('12px', StyleFontWeight.REGULAR, '1.4'),
}

export type FontsKey = keyof typeof Fonts
