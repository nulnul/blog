import { css } from '@emotion/react'

import React, { ButtonHTMLAttributes, PropsWithChildren } from 'react'
import { Colors } from '@/styles/colors'
import { Fonts } from '@/styles/fonts'
//import './button.css'

type ButtonSize = 46 | 44 | 40 | 38 | 32 | 26

export type ButtonColorType =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark'
  | 'link'

type StyleProps = {
  buttonColorType?: ButtonColorType
  size?: ButtonSize
  label?: string
}

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>, StyleProps {
  type?: 'button' | 'submit' | 'reset'
}

/**
 * Primary UI component for user interaction
 */
export const Button = ({
  type = 'button',
  size = 32,
  buttonColorType = 'primary',
  disabled = false,
  label,
  ...props
}: PropsWithChildren<ButtonProps>) => {
  const styleProps = { size, buttonColorType, disabled, label }
  return (
    <button css={styles.button(styleProps)} type={type} disabled={disabled} {...props}></button>
  )
}

export const styles = {
  button: ({ size, buttonColorType }: StyleProps) => css`
    font-family: 'Nunito Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    align-items: center;
    justify-content: center;
    vertical-align: middle;
    box-sizing: border-box;
    border: 0;
    border-radius: 1rem;
    cursor: pointer;
    display: inline-block;
    line-height: 1;
    min-width: 200px;
    min-height: ${size}px;
    padding-left: 12px;
    padding-right: 12px;
    ${Fonts.REGULAR_16}

    ${size == 40 &&
    css`
      min-width: 0;
      padding-left: 32px;
      padding-right: 32px;
    `}

    ${(size == 38 || size == 32 || size == 26) &&
    css`
      min-width: 0;
      ${Fonts.REGULAR_14}
    `}

    ${size == 26 &&
    css`
      padding: 3px 12px 2px;
      ${Fonts.REGULAR_12}
    `}

    ${buttonColorType == 'primary' &&
    css`
      background-color: ${Colors.PRIMARY};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.PRIMARY_70};
      }
    `}
    ${buttonColorType == 'secondary' &&
    css`
      background-color: ${Colors.SECONDARY};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.SECONDARY_70};
      }
    `}
    ${buttonColorType == 'success' &&
    css`
      background-color: ${Colors.SUCCESS};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.SUCCESS_70};
      }
    `}
    ${buttonColorType == 'danger' &&
    css`
      background-color: ${Colors.DANGER};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.DANGER_70};
      }
    `}
    ${buttonColorType == 'warning' &&
    css`
      background-color: ${Colors.WARNING};
      color: ${Colors.DARK_GRAY};
      &:not(:disabled):hover {
        background-color: ${Colors.WARNING_70};
      }
    `}
    ${buttonColorType == 'info' &&
    css`
      background-color: ${Colors.INFO};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.INFO_70};
      }
    `}
    ${buttonColorType == 'light' &&
    css`
      background-color: ${Colors.LIGHT};
      color: ${Colors.DARK_GRAY};
      &:not(:disabled):hover {
        background-color: ${Colors.DARK_20};
      }
    `}
    ${buttonColorType == 'dark' &&
    css`
      background-color: ${Colors.DARK};
      color: ${Colors.WHITE};
      &:not(:disabled):hover {
        background-color: ${Colors.DARK_70};
      }
    `}
    ${buttonColorType == 'link' &&
    css`
      background-color: ${Colors.WHITE};
      color: ${Colors.PRIMARY};
      &:not(:disabled):hover {
        text-decoration: underline;
        color: ${Colors.PRIMARY_70};
      }
    `}
  `,
}
