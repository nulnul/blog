import { Global } from '@emotion/react'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
// import './index.css'
import Root from '@/routes/Root'
import ErrorPage from '@/ErrorPage'
import Contact from '@/routes/Contact'
import globalStyle from '@/styles/global'

const router = createBrowserRouter([
  {
    path: `${import.meta.env.VITE_ROOT_PATH}/`,
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [],
  },
  {
    path: 'contacts/:contactId',
    element: <Contact />,
  },
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <>
    <Global styles={globalStyle} />
    <React.StrictMode>
      <RouterProvider router={router} />
    </React.StrictMode>
  </>
)
